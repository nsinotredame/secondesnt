# Internet

Vidéos pour aller plus loin sur le theme Internet 

## Dark nets/dark web/deep web

Vu que beaucoup d'entre vous me posent des questions sur ce domaine, voici des vidéos pour en savoir plus:

!!! info "Vidéos sur le côté technique des dark net/ du deep web"
    <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/zjqw1VnuKLs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

    <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/oiR2mvep_nQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

    <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/joxQ_XbsPVw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

!!! info "Ce qu'il se passe sur les dark nets"
    <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/GmRoqV5lxOI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

    <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/f8czGzZx2FU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
